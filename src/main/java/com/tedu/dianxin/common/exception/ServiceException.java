package com.tedu.dianxin.common.exception;

public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = -9085326160255400760L;

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}
}