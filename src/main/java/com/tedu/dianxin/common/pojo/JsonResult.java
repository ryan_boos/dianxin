package com.tedu.dianxin.common.pojo;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JsonResult implements Serializable{
	 private static final long serialVersionUID = 5110901796917551720L;
	/**状态吗*/
	 private Integer state=200;
	 /**状态信息*/
	 private String message="ok";
	 /**正常的响应数据*/
	 private Object data;
	 
	 public JsonResult(String message){
		 this.message=message;
	 }
	 public JsonResult(Object data){
		 this.data=data;
	 }
	 public JsonResult(Throwable e){//Throwable为所有异常类的父类
		 this.state=0;
		 this.message=e.getMessage();
	 }
}