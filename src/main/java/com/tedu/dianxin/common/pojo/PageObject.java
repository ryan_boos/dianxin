package com.tedu.dianxin.common.pojo;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageObject<T> implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = -4204125513201870346L;
	 /**总记录数(从数据库查询出来的)*/
	 private Integer rowCount;
	 /**总页数(基于rowCount和页面大小计算出来的)*/
	 private Integer pageCount;
	 /**页面大小(每页最多可以呈现的记录总数)*/
	 private Integer pageSize;
	 /**当前页码值(客户端传递)*/
	 private Integer pageCurrent;
	 /**当前页记录,list集合中的T由PageObject类上定义的泛型决定*/
	 private List<T> records;
	 public PageObject(Integer rowCount, Integer pageSize, Integer pageCurrent, List<T> records) {
		super();
		this.rowCount = rowCount;
		this.pageSize = pageSize;
		this.pageCurrent = pageCurrent;
		this.records = records;
		this.pageCount=(rowCount-1)/pageSize+1;
	 }
}
