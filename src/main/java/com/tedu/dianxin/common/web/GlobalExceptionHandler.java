package com.tedu.dianxin.common.web;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.tedu.dianxin.common.pojo.JsonResult;

@RestControllerAdvice
public class GlobalExceptionHandler {
	  @ExceptionHandler(RuntimeException.class)	  
	  public JsonResult doHandleRuntimeException(RuntimeException e) {		 
		  e.printStackTrace();
		  return new JsonResult(e);
	  }
}
