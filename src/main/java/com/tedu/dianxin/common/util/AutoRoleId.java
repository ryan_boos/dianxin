package com.tedu.dianxin.common.util;
import java.util.List;

public class AutoRoleId {

	public static Integer auto(List<Integer> list) {
		int tep = 100;
		int id = 0;
		for (Integer role : list) {
			if(role - tep != 0) {
				id = role - 100;
				break ;
			}else {
				id = role + 100;
			}
			tep += 100;
		}		
		return id;
	}
}
