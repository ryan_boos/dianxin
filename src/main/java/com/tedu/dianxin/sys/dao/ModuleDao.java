package com.tedu.dianxin.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.tedu.dianxin.sys.entity.ModuleInfoEntity;
import com.tedu.dianxin.sys.entity.RoleModuleEntity;

@Mapper
public interface ModuleDao {
	@Select("select mi.MODULE_ID ,mi.NAME\r\n"
			+ "from module_info mi left join role_module rm \r\n"
			+ "on mi.MODULE_ID = rm.MODULE_ID\r\n"
			+ "WHERE rm.ROLE_ID = #{id}")
	List<ModuleInfoEntity> findModuleInfoById(Integer id);
	
	@Select("select MODULE_ID , NAME from module_info")
	List<ModuleInfoEntity> findAll();
	
	int saveModuleInfoByIds(@Param("ids") List<RoleModuleEntity> ids);
	
	@Delete("delete from role_module where ROLE_ID = #{id}")
	int deleteModuleById(Integer id);
}
