package com.tedu.dianxin.sys.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AdminDao {

	@Select("select ADMIN_ID from admin_info")
	List<BigDecimal> findAllId();
	
	@Select("select count(role_id) from admin_role where role_id = #{id}")
	int findAdminRoleById(Integer id);
}
