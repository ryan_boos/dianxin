package com.tedu.dianxin.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.tedu.dianxin.sys.entity.RoleInfoEntity;



@Mapper
public interface RoleDao {

	@Select("select ROLE_ID, NAME  from role_info")
	List<RoleInfoEntity> getRoles();
	
	@Select("select ROLE_ID from role_info group by role_id  order by role_id ASC")
	List<Integer> getRoleInfoId();
	 	
	int saveRoleInfo(RoleInfoEntity roleInfo);
	
	@Delete("delete from role_info where ROLE_ID = #{id}")
	int deleteRoleById(Integer id);

	@Update("update role_info set name=#{roleName} where role_id = #{roleId}")
	int updateRoleNameById(String roleName,Integer roleId);
}
