package com.tedu.dianxin.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PagesController {

	@GetMapping("{model}/{page}")
	public String showAll(@PathVariable String model,@PathVariable String page) {		
		return model+"/"+page;
	}
	
	@GetMapping("{page}")
	public String ShowPage(@PathVariable String page) {		
		return page;
	}
	
	@GetMapping("/favicon.ico")
	@ResponseBody
	public String logo() {
		return "chrome_logo";
	}
}
