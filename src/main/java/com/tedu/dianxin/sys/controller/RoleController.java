package com.tedu.dianxin.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tedu.dianxin.common.pojo.JsonResult;
import com.tedu.dianxin.common.pojo.PageObject;
import com.tedu.dianxin.sys.entity.Vo.RoleListVo;
import com.tedu.dianxin.sys.entity.Vo.RoleSubmitParamVo;
import com.tedu.dianxin.sys.service.RoleService;

@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private RoleService roleService;

	/**
	 * 请求table页面和页面
	 * @param current 当前页
	 * @param size 页面大小
	 * @return table数据和状态
	 */
	@GetMapping("list")
	public JsonResult getList(@RequestParam(defaultValue = "1") Integer current, @RequestParam(defaultValue = "20") Integer size) {
		System.out.println(current + " :::::: " + size);
		PageObject<RoleListVo> objects = roleService.getObjects(current, size);
		return new JsonResult(objects);
	}
	/**
	 * 获取权限列表
	 * @return 权限列表
	 */
	@GetMapping("moduleInfo")
	public JsonResult getMoudleInfo() {
		return roleService.getAllModuleIfon();
	}
	
	/**
	 * 添加角色
	 * @param param 请求参数 roleId:为空,roleName:角色名称,moduleInfo:权限列表
	 * @return 存入后发生状态码和错误信息. 200成功, 0失败, 
	 */
	@PostMapping(value = "add/list", produces = "application/json;charset=UTF-8")
	public JsonResult addList(@RequestBody RoleSubmitParamVo<Integer> param) {	
		return roleService.addDataList(param);
	}
	/**
	 * 修改角色
	 * @param param 请求参数 roleId:角色ID,roleName:角色名称,moduleInfo:权限列表
	 * @return 存入后发生状态码和错误信息. 200成功, 0失败, 
	 */
	@PostMapping(value = "upadte/list", produces = "application/json;charset=UTF-8")
	public JsonResult updateList(@RequestBody RoleSubmitParamVo<Integer> param) {		
		System.out.println(param);
		return roleService.updateDataList(param);
	}
	
	@DeleteMapping(value = "delete/{id}")
	public JsonResult deleteById(@PathVariable("id") Integer id ) {
		return roleService.deleteById(id);
	}
	
}
