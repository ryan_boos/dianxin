package com.tedu.dianxin.sys.service;

import com.tedu.dianxin.common.pojo.JsonResult;
import com.tedu.dianxin.common.pojo.PageObject;
import com.tedu.dianxin.sys.entity.Vo.RoleListVo;
import com.tedu.dianxin.sys.entity.Vo.RoleSubmitParamVo;

public interface RoleService {
	PageObject<RoleListVo> getObjects(Integer current, Integer size);
	
	JsonResult getAllModuleIfon();

	JsonResult addDataList(RoleSubmitParamVo<Integer> param);

	JsonResult updateDataList(RoleSubmitParamVo<Integer> param);

	JsonResult deleteById(Integer id);
}
