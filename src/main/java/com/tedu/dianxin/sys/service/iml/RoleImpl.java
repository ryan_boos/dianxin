package com.tedu.dianxin.sys.service.iml;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tedu.dianxin.common.exception.ServiceException;
import com.tedu.dianxin.common.pojo.JsonResult;
import com.tedu.dianxin.common.pojo.PageObject;
import com.tedu.dianxin.common.util.AutoRoleId;
import com.tedu.dianxin.sys.dao.AdminDao;
import com.tedu.dianxin.sys.dao.ModuleDao;
import com.tedu.dianxin.sys.dao.RoleDao;
import com.tedu.dianxin.sys.entity.ModuleInfoEntity;
import com.tedu.dianxin.sys.entity.RoleInfoEntity;
import com.tedu.dianxin.sys.entity.RoleModuleEntity;
import com.tedu.dianxin.sys.entity.Vo.RoleListVo;
import com.tedu.dianxin.sys.entity.Vo.RoleSubmitParamVo;
import com.tedu.dianxin.sys.service.RoleService;

@Service
public class RoleImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private ModuleDao moduleDao;
	
	@Autowired
	private AdminDao adminDao;

	/**
	 * 显示所有的角色信息
	 */
	@Override
	public PageObject<RoleListVo> getObjects(Integer current, Integer size) {
		// TODO Auto-generated method stub
		if (current == null || current < 1) {
			throw new IllegalArgumentException("无效的当前页!");
		}
		if (size == null || size < 1 || size > 100) {
			throw new IllegalArgumentException("无效的页面值");
		}
		PageHelper.startPage(current,size);
			
		List<RoleInfoEntity> roles = roleDao.getRoles();
		
		PageInfo<RoleInfoEntity> pageInfo = new PageInfo<>(roles);
		
		if (roles.isEmpty()) {
			throw new ServiceException("已经没有角色数据了");
		}
		
		List<RoleListVo> list = new ArrayList<>();

		for (RoleInfoEntity role : roles) {
			List<ModuleInfoEntity> findModuleInfoById = moduleDao.findModuleInfoById(role.getRoleId().intValue());
			list.add(new RoleListVo(role.getRoleId(), role.getName(), findModuleInfoById));
		}
		  
		Integer rowCount = (int)pageInfo.getTotal();
		Integer pageSize = pageInfo.getPageSize();
		Integer  pageCurrent = pageInfo.getPageNum();
		
		return new PageObject<>( rowCount , pageSize, pageCurrent, list);
	}

	@Override
	public JsonResult getAllModuleIfon() {
		// TODO Auto-generated method stub		
		return new JsonResult(moduleDao.findAll());
	}

	@Override
	public JsonResult addDataList(RoleSubmitParamVo<Integer> param) {
		// TODO Auto-generated method stub
		if(param == null) {
			throw new ServiceException("参数不能为空");
		}
		if(param.getRoleName()==""||param.getRoleName()==null) {
			throw new ServiceException("角色名不能为空!");
		}
		if(param.getModuleInfo().size()<1||param.getModuleInfo().isEmpty()) {
			throw new ServiceException("至少选择一个权限");
		}
		Integer id = AutoRoleId.auto( roleDao.getRoleInfoId()); //自动生成roleId
		if(id == null ) {
			throw new ServiceException("数据库异常,请联系管理员!");
		}
		RoleInfoEntity roleInfo = new RoleInfoEntity(new BigDecimal(id),param.getRoleName());
		int saveRoleInfo = roleDao.saveRoleInfo(roleInfo);
		
		if(saveRoleInfo<1) {
			throw new ServiceException("角色名称保存失败");
		}
		
		List<RoleModuleEntity> list = new ArrayList<>(); 
		
		for(Integer moduleId : param.getModuleInfo()) {
			list.add(new RoleModuleEntity(new BigDecimal(id) ,new BigDecimal(moduleId)));
		}
		
		int saveModuleInfoByIds = moduleDao.saveModuleInfoByIds(list);
		if(saveModuleInfoByIds<1) {
			throw new ServiceException("权限列表保存失败");
		}
		
		return new JsonResult("角色保存成功!");
	}

	@Override
	public JsonResult updateDataList(RoleSubmitParamVo<Integer> param) {
		// TODO Auto-generated method stub
		if(param == null) {
			throw new ServiceException("参数不能为空");
		}
		if(param.getRoleId() == null || param.getRoleId() == 0) {
			
		}
		if(param.getRoleName()==""||param.getRoleName()==null) {
			throw new ServiceException("角色名不能为空!");
		}
		if(param.getModuleInfo().size()<1||param.getModuleInfo().isEmpty()) {
			throw new ServiceException("至少选择一个权限");
		}
		int upadteRole =roleDao.updateRoleNameById(param.getRoleName(),param.getRoleId());
		if(upadteRole<1) {
			throw new ServiceException("角色名称修改失败");
		}
		int moduleDelete = moduleDao.deleteModuleById(param.getRoleId());
		if(moduleDelete < 1) throw new ServiceException("删除权限失败");

		List<RoleModuleEntity> list = new ArrayList<>(); 
		
		for(Integer moduleId : param.getModuleInfo()) {
			list.add(new RoleModuleEntity(new BigDecimal(param.getRoleId()) ,new BigDecimal(moduleId)));
		}
		
		int saveModuleInfoByIds = moduleDao.saveModuleInfoByIds(list);
		if(saveModuleInfoByIds<1) {
			throw new ServiceException("权限列表保存失败");
		}
			
		return new JsonResult("角色修改成功!");
	}

	@Override
	public JsonResult deleteById(Integer id) {
		// TODO Auto-generated method stub
		if(id==null)throw new ServiceException("无效的删除ID");
		int findAdminRoleById = adminDao.findAdminRoleById(id);
		if(findAdminRoleById>0) throw new ServiceException("删除错误！该角色被使用，不能删除。");
		int roleDelete = roleDao.deleteRoleById(id);
		if(roleDelete < 1) throw new ServiceException("删除角色id失败");
		int moduleDelete = moduleDao.deleteModuleById(id);
		if(moduleDelete < 1) throw new ServiceException("删除权限失败");
		return new JsonResult("删除角色成功!");
	}

}
