package com.tedu.dianxin.sys.entity;


import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author ryan
 * @email 641483856@qq.com
 * @date 2021-01-01 08:26:57
 */
@Data
public class CostEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	private BigDecimal costId;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private BigDecimal baseDuration;
	/**
	 * 
	 */
	private BigDecimal baseCost;
	/**
	 * 
	 */
	private BigDecimal unitCost;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String descr;
	/**
	 * 
	 */
	private Date creatime;
	/**
	 * 
	 */
	private Date startime;
	/**
	 * 
	 */
	private String costType;

}
