package com.tedu.dianxin.sys.entity.Vo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RoleSubmitParamVo <T>{
	private Integer roleId;
	
	private String  roleName;
	
	private List<T> moduleInfo;

}
