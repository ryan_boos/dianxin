package com.tedu.dianxin.sys.entity;


import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author ryan
 * @email 641483856@qq.com
 * @date 2021-01-01 08:26:57
 */
@Data

public class AdminInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	
	private BigDecimal adminId;
	/**
	 * 
	 */
	private String adminCode;
	/**
	 * 
	 */
	private String password;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String telephone;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	private Date enrolldate;

}
