package com.tedu.dianxin.sys.entity;



import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * @author ryan
 * @email 641483856@qq.com
 * @date 2021-01-01 08:26:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	
	private BigDecimal accountId;
	/**
	 * 
	 */
	private BigDecimal recommenderId;
	/**
	 * 
	 */
	private String loginName;
	/**
	 * 
	 */
	private String loginPasswd;
	/**
	 * 
	 */
	private Integer status;
	/**
	 * 
	 */
	private Date createDate;
	/**
	 * 
	 */
	private Date pauseDate;
	/**
	 * 
	 */
	private Date closeDate;
	/**
	 * 
	 */
	private String realName;
	/**
	 * 
	 */
	private String idcardNo;
	/**
	 * 
	 */
	private Date birthdate;
	/**
	 * 
	 */
	private String gender;
	/**
	 * 
	 */
	private String occupation;
	/**
	 * 
	 */
	private String telephone;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	private String mailaddress;
	/**
	 * 
	 */
	private String zipcode;
	/**
	 * 
	 */
	private String qq;
	/**
	 * 
	 */
	private Date lastLoginTime;
	/**
	 * 
	 */
	private String lastLoginIp;

}
