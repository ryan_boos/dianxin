package com.tedu.dianxin.sys.entity;



import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * @author ryan
 * @email 641483856@qq.com
 * @date 2021-01-01 08:26:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private BigDecimal roleId;
	/**
	 * 
	 */
	private String name;

}
