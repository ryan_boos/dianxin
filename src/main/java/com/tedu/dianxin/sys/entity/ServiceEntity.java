package com.tedu.dianxin.sys.entity;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author ryan
 * @email 641483856@qq.com
 * @date 2021-01-01 08:26:57
 */
@Data
public class ServiceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private BigDecimal serviceId;
	/**
	 * 
	 */
	private BigDecimal accountId;
	/**
	 * 
	 */
	private String unixHost;
	/**
	 * 
	 */
	private String osUsername;
	/**
	 * 
	 */
	private String loginPasswd;
	/**
	 * 
	 */
	private Integer status;
	/**
	 * 
	 */
	private Date createDate;
	/**
	 * 
	 */
	private Date pauseDate;
	/**
	 * 
	 */
	private Date closeDate;
	/**
	 * 
	 */
	private BigDecimal costId;

}
