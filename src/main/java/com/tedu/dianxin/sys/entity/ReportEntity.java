package com.tedu.dianxin.sys.entity;



import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author ryan
 * @email 641483856@qq.com
 * @date 2021-01-01 08:26:57
 */
@Data
public class ReportEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer reportId;
	/**
	 * 
	 */
	private BigDecimal serviceId;
	/**
	 * 
	 */
	private Date loginTime;
	/**
	 * 
	 */
	private Date logoutTime;

}
