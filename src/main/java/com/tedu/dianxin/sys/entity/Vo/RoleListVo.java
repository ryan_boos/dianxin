package com.tedu.dianxin.sys.entity.Vo;

import java.math.BigDecimal;
import java.util.List;

import com.tedu.dianxin.sys.entity.ModuleInfoEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@AllArgsConstructor
public class RoleListVo {
	private BigDecimal roleId; //角色ID
	
	private String roleName;  //角色名
	
	private  List<ModuleInfoEntity>  moduleInfo; //角色权限	
	
}
