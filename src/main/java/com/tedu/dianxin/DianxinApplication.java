package com.tedu.dianxin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DianxinApplication {

	public static void main(String[] args) {
		SpringApplication.run(DianxinApplication.class, args);
	}

}
