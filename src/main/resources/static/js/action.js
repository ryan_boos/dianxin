window.onload = function() {
	// 导航栏图标切换效果
	var nav = document.querySelectorAll("#menu li a");
	var path = this.location.pathname;
	nav.forEach(item => {
		var name = item.className.split('_')[0];
		if (item.pathname == path) {			
			item.className = name + "_on"
		} else {
			// 鼠标指向
			item.onmouseover = function() {				
				item.className = name + "_on"
			};
			// 鼠标离开
			item.onmouseout = function() {				
				item.className = name + "_off"
			};
		}
	});
}