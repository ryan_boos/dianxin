package com.tedu.dianxin.databases;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.tedu.dianxin.sys.dao.ModuleDao;
import com.tedu.dianxin.sys.dao.RoleDao;
import com.tedu.dianxin.sys.entity.ModuleInfoEntity;

import lombok.extern.log4j.Log4j;

@SpringBootTest
public class DataSource {

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private ModuleDao moduleDao;

	@Test
	public void contextLoads() {

		List<ModuleInfoEntity> find = moduleDao.findModuleInfoById(100);

		System.out.println(find.toString());
	}

	@Test
	public void cone() {
		// Integer roleInfoId = roleDao.getRoleInfoId();
		Integer[] arr = { 100, 300, 400, 500, 800 };
		int tep = 100;
		int s = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] - tep != 0) {
				s = arr[i] - 100;
				return;
			}
			tep += 100;
		}
		System.out.println(s + ":::::::::::::::::::::::::");

		// System.out.println(roleInfoId);

	}
	

}
